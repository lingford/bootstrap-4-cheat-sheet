# Bootstrap 4 Cheat Sheet

### A list of example Bootstrap 4 classes and components

*Many classes have changed from Bootstrap version 3 to version 4*

- New `.classes` have been added, see [**https://getbootstrap.com/docs/4.3/migration/**](https://getbootstrap.com/docs/4.3/migration/)
- Many Bootstrap 3 `.classes` e.g. `.panel, .well, .label` have been replaced or altered. 
- This cheat sheet should help you upgrade your new or existing site to Bootstrap 4!
